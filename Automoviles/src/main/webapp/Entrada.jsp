<%-- 
    Document   : Entrada
    Created on : 10/06/2020, 08:12:51 PM
    Author     : toti0
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
        
        <title>Nuevo Modelo</title>
        
            <style>
                
        p[name = "a" ], p[name = "b" ], p[name = "c" ], p[name = "d" ] {
            
            text-decoration: none;
            padding: 5px;
            padding-left: 0px;
            padding-right: 0px;
            font-family: Franklin Gothic Medium;
            font-weight: 300;
            font-size: 20px;
            font-style: normal;
            color: #000000;
            background-color: #ffffff;
            border-radius: 10px;
            border: 5px solid #000000;
            
        }
        
        h1{
            font-family: Franklin Gothic Medium;
            font-weight: 300;
            font-size: 40px;
            font-style: normal;
            color: #000000;    
            border: 5px solid #000000;
            border-radius: 5px;
            background-color: #ffffff;
        }
        
        input[name="Marca"],input[name="Modelo"], input[name="Anio"], input[name="Color"], input[name="Valor"] {
           
            color: #000000;    
            border: 5px solid #000000;
            border-radius: 5px;
            background-color: #ffffff;
        }
        
        
    </style>
        
        
    </head>
    <body background="fondor.jpg">
        
        <h1><center><strong><font color="black">NUEVO MODELO</font></strong></center></h1>
        
        
        <form name="Registro" action="data.jsp" method="get">
		
            <center>
            
            
            <p name = "a"><strong>Escribe la marca </strong></p>
            <input type="text" name="Marca" style="WIDTH: 350px; HEIGHT: 50px">
            
            <p name = "b"><strong>Escribe el modelo </strong></p>
            <input type="text" name="Modelo" style="WIDTH: 350px; HEIGHT: 50px">

            <p name = "c"><strong>Introduce el a�o </strong></p>
            <input type="number" min="1900" max="2099" step="1" value="2016" name="Anio" style="WIDTH: 350px; HEIGHT: 50px"/>
            
            <p name = "d"><strong>Escribe el color </strong></p>
            <input type="text" name="Color" style="WIDTH: 350px; HEIGHT: 50px">
            
            <p name = "d"><strong>Escribe el valor estimado </strong></p>
            <input type="number" step="0.01" min="0" name="Valor" style="WIDTH: 350px; HEIGHT: 50px">
			

            
            <br/>
            <br/>
            <br/>
            
            <input type=image src="flecha.png" style="max-width:10%;width:auto;height:auto;">
            
            </center>
            
        </form>

                
    <center>
        
    </br>
        <a href="index.html">
        <img src="home.png" style="max-width:7%;width:auto;height:auto;">
        </a>
    
    </center>	

        
        
        
    </body>
</html>
