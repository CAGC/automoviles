<%-- 
    Document   : Editar
    Created on : 11/06/2020, 02:39:18 PM
    Author     : toti0
--%>


<%@page contentType="text/html" pageEncoding="ISO-8859-1"
        import ="java.sql.Connection"        
        import ="java.sql.DriverManager"        
        import ="java.sql.ResultSet"        
        import ="java.sql.Statement"        
        import ="java.sql.SQLException"        

        %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Modificar</title>
        
        <style>
                
        p, button{
            
            text-decoration: none;
            padding: 5px;
            padding-left: 0px;
            padding-right: 0px;
            font-family: Franklin Gothic Medium;
            font-weight: 300;
            font-size: 20px;
            font-style: normal;
            color: #000000;
            background-color: #ffffff;
            border-radius: 10px;
            border: 5px solid #000000;
            
        }
        
        h1{
            font-family: Franklin Gothic Medium;
            font-weight: 300;
            font-size: 40px;
            font-style: normal;
            color: #000000;    
            border: 5px solid #000000;
            border-radius: 5px;
            background-color: #ffffff;
        }
        
        table,th,td{
           
            color: #000000;    
            border: 5px solid #000000;
            border-radius: 5px;
            background-color: #ffffff;
            font-family: Franklin Gothic Medium;
            font-weight: 300;
            font-size: 20px;
            font-style: normal;
        }
        
        table {
            border-collapse: collapse;
            width: 100%;
        }
        
        input[name="Id"], input[name="Marca"],input[name="Modelo"], input[name="Anio"], input[name="Color"], input[name="Valor"] {
           
            color: #000000;    
            border: 5px solid #000000;
            border-radius: 5px;
            background-color: #ffffff;
        }
        
        
    </style>
    
    </head>
    <body background="fondovf.jpg">
        <h1><strong><center>Edici�n de registros</center></strong></h1>
        
    <center>
        <table >
            
            <p><strong>�Qu� automovil deseas editar?</strong></p>
            
            <tr>
                <th>ID</th>
                <th>MARCA</th>
                <th>MODELO</th>
                <th>A�O</th>
                <th>COLOR</th>
                <th>VALOR</th>
                <th>FECHA DE REGISTRO</th>
            </tr>
          
            <%
                Connection conex = null;
                Statement sql = null;
                
                try {
                    Class.forName("com.mysql.cj.jdbc.Driver");
                    conex = (Connection) DriverManager.getConnection("jdbc:mysql://127.0.0.1/Automovil?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true&useSSL=false", "root", "root");
                    sql = conex.createStatement();

                    String qry = "select * from Automovil.registros";
                    ResultSet data = sql.executeQuery(qry);
                    while (data.next()) {
            %>
            
            
            <% if(data.getInt("Visibilidad") == 1){ %>
            <tr>
                <td>
                    <% out.print(data.getInt("Id"));%>
                </td>
                <td>
                    <% out.print(data.getString("Marca"));%>
                </td>
                <td>
                    <% out.print(data.getString("Modelo"));%>
                </td>
                
                <td>
                    <% out.print(data.getString("Anio"));%>
                </td>
                
                <td>
                    <% out.print(data.getString("Color"));%>
                </td>
                
                <td>
                    <%  out.print("$ ");
                        out.print(data.getString("Valor"));%>
                </td>
                
                <td>
                    <% out.print(data.getString("Fecha de creaci�n"));%>
                </td>
                

            </tr>

            <%  } 
                    }
                    data.close();

                } catch (Exception e) {
                    out.print("Error en la conexi�n con los registros.");
                    e.printStackTrace();
                }

            %>
      
        </table>
        
        </center>
            <form name="Registro" action="Sobreescribir.jsp" method="get">

        <center>
            
            <p><strong>
            Ingresa el ID del auto a modificar &nbsp; <input type="text" name="Id" value="" style="WIDTH: 350px; HEIGHT: 30px"/> </strong></p>
        
            <p name = "a"><strong>Escribe la marca &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            <input type="text" name="Marca" style="WIDTH: 350px; HEIGHT: 30px"></strong></p>
            
            <p name = "b"><strong>Escribe el modelo &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
            <input type="text" name="Modelo" style="WIDTH: 350px; HEIGHT: 30px"></strong></p>

            <p name = "c"><strong>Introduce el a�o &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
            <input type="number" min="1900" max="2099" step="1" value="2016" name="Anio" style="WIDTH: 350px; HEIGHT: 30px"/></strong></p>
            
            <p name = "d"><strong>Escribe el color &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            <input type="text" name="Color" style="WIDTH: 350px; HEIGHT: 30px"></strong></p>
            
            <p name = "d"><strong>Escribe el valor estimado &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            <input type="number" step="0.01" min="0" name="Valor" style="WIDTH: 350px; HEIGHT: 30px"></strong></p>
            
            <br/>

            <br/>

            
            <input type=image src="flecha.png" style="max-width:10%;width:auto;height:auto;">
            
            
            </center>
        </form>>       
        
    <center>
    
    <button onclick="window.location.href = 'Eliminar.jsp';">Eliminar un auto</button>

    </br>
        <a href="index.html">
        <img src="home.png" style="max-width:7%;width:auto;height:auto;">
        </a>
    
    </center>  
    
    
    
    
    </body>
</html>
